# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Where to find the notes? ###
Thesis Sentiment Analysis Code (on Bear)

#core #thesis/code
1. Use Test.py
2. Use these configuration
	For SVM:
	` vectorizer = joblib.load("./pickles/tfidf_vectorizer_x1_07_03_2018.pkl")`
	`ch2 = joblib.load("./pickles/chi2Best_x1_07_03_2018.pkl")`
	`clf = joblib.load("./pickles/svm_x1_07_03_2018.pkl")`
3. Change Date on Label Data



### MOST IMPORTANT THINGS:
1. Don't forget to check the FPR from ROC!!!!!
2. dan ternyata C=100, kernel=rbf, gamma=auto, and class_weight = class_weight={0: 5.5, 1:3}
	--> This creates the best, lean model of SVR 
	1. beta 1. C=100, gamma=auto, class_weight={0: 5.5, 1:3} ==> joblib.dump(clf,"./pickles/ROC_based/roc_svm_beta1.pkl")
	2. beta 2. C=100, gamma=auto, class_weight={0: 5.25, 1:3} ==> joblib.dump(clf,"./pickles/ROC_based/roc_svm_beta2.pkl")
	3. beta 3. C=5500, gamma=auto, class_weight={0: 5.5, 1:3} ==> joblib.dump(clf,"./pickles/ROC_based/roc_svm_beta3.pkl")